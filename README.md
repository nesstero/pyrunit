[![PyPI version](https://badge.fury.io/py/pyrunit.svg)](https://badge.fury.io/py/pyrunit)
# PyRunit
Simple shortcut to manage runit service.

# Install
```
$ pip install pyrunit
```

# Usage
```
$ pyrunit -h

██████╗ ██╗   ██╗██████╗ ██╗   ██╗███╗   ██╗██╗████████╗
██╔══██╗╚██╗ ██╔╝██╔══██╗██║   ██║████╗  ██║██║╚══██╔══╝
██████╔╝ ╚████╔╝ ██████╔╝██║   ██║██╔██╗ ██║██║   ██║
██╔═══╝   ╚██╔╝  ██╔══██╗██║   ██║██║╚██╗██║██║   ██║
██║        ██║   ██║  ██║╚██████╔╝██║ ╚████║██║   ██║
╚═╝        ╚═╝   ╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚═╝   ╚═╝ v.0.1.0
Simple shortcut to manage runit service.


Options:
  -h, --help               Display this message
  -l, --list-service       Enable service list
  -la, --list-all-service  List a service
  -c, --check-status       Check service status
  -r, --run                Run service
  -s, --stop               Stop service
  -R, --restart            Restart service
  -e, --enable             Enable service
  -d, --disable            Disable service
```
