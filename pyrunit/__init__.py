#! /usr/bin/env python
from .pyrunit import runit

def pyRunit():
    Runit = runit()
    Runit.pyrunit()
