#! /usr/bin/env python

from setuptools import find_packages, setup

with open('README.md', 'r') as des:
    l_desc = des.read()

setup(
    name='pyrunit',
    packages=find_packages(),
    version='0.1.0',
    entry_points={'console_scripts': ['pyrunit = pyrunit:pyRunit']},
    description='Simple shortcut to manage runit service',
    url='https://gitlab.com/nesstero/pyrunit',
    license='MIT',
    author='nestero',
    author_email='nestero@mail.com',
    long_description=l_desc,
    long_description_content_type='text/markdown',
)
